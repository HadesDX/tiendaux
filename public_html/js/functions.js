var rsURL = "/tiendaPHP/public_html/ws/";
var imgURL = "/tiendaPHP/public_html/items/";
/**
 * 
 * @param {JQuery} form
 * @returns {Object}
 */
function formToObject(form) {

    var o = {};
    form.find('[name]').each(function () {
        var t = $(this);
        if (t.attr("type") === "radio" || t.attr("type") === "checkbox") {
            if (t.prop("checked")) {
                var name = t.attr('name');
                o[name] = t.val();
            }
        } else {
            var name = t.attr('name');
            o[name] = t.val();
        }
    });

    return o;
}

function loadPage(event) {
    var m = $('main');
    m.empty();
    if (event.data) {
        m.load(event.data.url, event.data, event.data.callback);
    } else {
        console.error("No data object in event");
    }
}
function getExpire() {
    var minutes = 60 * 4;
    var date = new Date();
    date.setTime(date.getTime() + (minutes * 60 * 1000));
}
function getNumberItemsInCart() {
    if (SHOP.cart) {
        var c = 0;
        for (var i in SHOP.cart) {
            c += parseInt(SHOP.cart[i].qty);
        }
        return c;
    }
    return 0;
}
function addToCart(item) {
    item.qty = parseInt(item.qty);
    var o = SHOP.cart.find(function (object, index) {
        if (object.item.id_item == item.item.id_item) {
            return true;
        }
    });
    if (o) {
        console.log(o);
        console.log(item);
        o.item = item.item;
        o.qty = parseInt(o.qty) + parseInt(item.qty);
        if (o.qty > item.item.number) {
            o.qty = parseInt(item.item.number);
        }
    } else {
        SHOP.cart.push(item);
    }
    console.log(SHOP.cart);
    $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
    $('[href="#cart"] .badge').text(getNumberItemsInCart());
}
function emptyCart() {
    SHOP.cart = [];
    $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
    $('[href="#cart"] .badge').text(getNumberItemsInCart());
}
function setCart(dataArray) {
    SHOP.cart = dataArray;
    $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
    $('[href="#cart"] .badge').text(getNumberItemsInCart());
}
function removeFromCart(index) {
    SHOP.cart.splice(index, 1);
    $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
    $('[href="#cart"] .badge').text(getNumberItemsInCart());
}
function removeOneFromCart(index) {
    var qty = parseInt(SHOP.cart[index].qty);
    if (qty > 1) {
        --qty;
        SHOP.cart[index].qty = qty;
        $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
        $('[href="#cart"] .badge').text(getNumberItemsInCart());
        return true;
    }
    return false;
}
function addOneToCart(index) {
    var qty = parseInt(SHOP.cart[index].qty);
    var number = parseInt(SHOP.cart[index].item.number);
    if (qty < number) {
        ++qty;
        SHOP.cart[index].qty = qty;
        $.cookie('cart' + SHOP.user.id_user, JSON.stringify(SHOP.cart), {expires: getExpire()});
        $('[href="#cart"] .badge').text(getNumberItemsInCart());
        return true;
    }
    return false;
}